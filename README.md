# The movie dB

## Launch

First, install dependencies with yarn or npm and then run `yarn start` or `npm start` from the root of the project.

You can start the app on any web server, just specify the `src` directory as a webroot.

## Source code

JavaScript is written using modules, async/await, arrow functions, destructuring, Proxy, classes, template strings, etc.

CSS is split into modules as well and injected via an `@import` directive. There are CSS Grid and CSS Variables used.

Please notice `src/js/config.js` that contains The Movie DB API settings and even the API key.
It mustn't be there in public, but I committed it for easier application launch.

## Components

There are 2 components (`Dropdown and SearchField`) that consist of markup (inside `index.html`), style sheets (inside `src/css` directory) and logic (inside `src/js` dirctory).

Each component provides a functionality to get a value (`getValue()` method) and to subscribe to value changes (`setChangeListener()` method).

`Dropdown` supports an alignment class modifier (`.dropdown--left` or `.dropdown--right`).

`SearchField` debounces an `input` event with a delay of 300ms.

## Data source

`src/js/overfetch.js` contains `overfetch` function that's just simple wrapper over the `window.fetch`. It constructs a correct request URI, binds params, handles errors, and transforms response payload.

`src/js/dataSources.js` exports 3 functions;

- `getImagesBaseUri` — loads API configuration and returns base images URI
- `getRecentMovies` — loads movies released during the last 3 months
- `searchMovie` — finds movies by title

## Render

`src/js/renderMoviesList.js` contains the main render function `renderMovie` that constructs DOM and attaches required events.

## Filters

Filters are encapsulated into the `Filters` class that works with `Dropdown` and `SearchField` described above to listen for filters changes.

## App

`App` it's like a front-controller. It's responsible for state managing, rendering, and event handling.
The state is wrapped into `Proxy` and change to the state leads to rendering a movie list.
When setting new state I use destructuring to clone an object, because there is an `Object.is` test on a previous and new state.
