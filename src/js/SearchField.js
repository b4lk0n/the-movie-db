import { debounce } from './utils.js';

const SEARCH_FIELD_INPUT_SELECTOR = '.search-field__input';
const SEARCH_FIELD_CLEAR_SELECTOR = '.search-field__icon-clear';
const SEARCH_FIELD_DEBOUNCE_DELAY = 300;

export class SearchField {
  constructor(selector) {
    this.root = document.querySelector(selector);

    if (!this.root) {
      throw new Error(
        `Could not find root search field element at '${selector}'`
      );
    }

    this.input = this.root.querySelector(SEARCH_FIELD_INPUT_SELECTOR);

    if (!this.input) {
      throw new Error(
        `Could not find search field input element at '${selector} ${SEARCH_FIELD_INPUT_SELECTOR}'`
      );
    }

    // need to debounce "input" event to prevent unnecessary API requests
    this.input.addEventListener(
      'input',
      debounce(this.handleSearchChange, SEARCH_FIELD_DEBOUNCE_DELAY)
    );
    this.clear = this.root.querySelector(SEARCH_FIELD_CLEAR_SELECTOR);

    if (!this.clear) {
      throw new Error(
        `Could not find search field clear icon element at '${selector} ${SEARCH_FIELD_CLEAR_SELECTOR}'`
      );
    }

    this.clear.addEventListener('click', this.handleClearClick);
  }

  handleSearchChange = (event) => {
    if (this.onChange) {
      this.onChange(event.target.value);
    }
  };

  handleClearClick = () => {
    this.input.value = '';
    // to be sure that "input" event listeners will be notified about this change
    this.input.dispatchEvent(new Event('input'));
  };

  setChangeListener(listener) {
    if ('function' !== typeof listener) {
      throw new TypeError('Change listener must be a function');
    }

    this.onChange = listener;
  }

  getValue() {
    return this.input.value;
  }
}
