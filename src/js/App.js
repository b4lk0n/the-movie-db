import { Filters } from './Filters.js';
import {
  getImagesBaseUri,
  getRecentMovies,
  searchMovie,
} from './dataSource.js';
import { renderMoviesList } from './renderMoviesList.js';

export class App {
  constructor(filters, elements) {
    this.root = elements.root;
    this.filterTitle = elements.filterTitle;
    this.moviesCountFiltered = elements.moviesCountFiltered;
    this.moviesCountTotal = elements.moviesCountTotal;

    if (!(filters instanceof Filters)) {
      throw new TypeError('Filters must be an instance of Filters class');
    }

    this.filters = filters;
    this.filters.setChangeListener(this.handleFiltersChange);
    this.viewData = new Proxy(
      {
        state: {
          isLoading: true,
          movies: null,
          filtered: null,
          errorMessage: '',
          favorites: Object.keys(localStorage)
            .map((id) => Number.parseInt(id, 10))
            .filter((id) => id > 0),
        },
      },
      {
        set: this.handleStateSet,
      }
    );
  }

  handleStateSet = (target, prop, value) => {
    // do nothing if value didn't change
    if (Object.is(Reflect.get(target, prop), value)) {
      return true;
    }

    const result = Reflect.set(target, prop, value);
    this.render();

    return result;
  };

  handleToggleFavorites = (movieId) => {
    const nextState = { ...this.viewData.state };
    const isSaved = !!localStorage.getItem(movieId);

    if (isSaved) {
      localStorage.removeItem(movieId);
      // we need to duplicate "favorites" state in memory
      // to be able to re-render when user modifies his favorites list
      nextState.favorites = nextState.favorites.filter((id) => movieId !== id);
    } else {
      localStorage.setItem(movieId, '1');
      nextState.favorites = [...nextState.favorites, movieId];
    }

    this.viewData.state = nextState;
  };

  handleFiltersChange = (filters) => this.fetchMovies(filters);

  async fetchMovies({ sortBy, showMode, searchQuery }) {
    const nextState = {
      ...this.viewData.state,
      isLoading: false,
    };

    try {
      nextState.movies = searchQuery.length
        ? await searchMovie(searchQuery)
        : await getRecentMovies(sortBy === 'rating');

      nextState.filtered =
        showMode !== 'favorites'
          ? nextState.movies
          : nextState.movies.filter((movie) =>
              nextState.favorites.includes(movie.id)
            );
    } catch (e) {
      nextState.errorMessage = e.message;
    }

    this.viewData.state = nextState;
  }

  getValue() {
    return {
      ...this.viewData.state,
    };
  }

  render() {
    const value = this.getValue();

    // render counters
    this.moviesCountTotal.textContent = value.movies.length;
    this.moviesCountFiltered.textContent = value.filtered.length;

    // filters title
    const { showMode } = this.filters.getValue();
    this.filterTitle.textContent =
      showMode === 'all' ? 'All movies' : 'Favorites';

    // render results
    const newChildren = renderMoviesList(
      value,
      this.getImagesBaseUri,
      this.handleToggleFavorites
    );
    this.root.innerHTML = '';
    this.root.appendChild(newChildren);
  }

  async start() {
    this.getImagesBaseUri = await getImagesBaseUri();
    await this.fetchMovies(this.filters.getValue());
  }
}
