import { API_BASE_URI, API_KEY } from './config.js';
import { trim, pick } from './utils.js';

function buildUri(path, params = {}) {
  let uri = `${API_BASE_URI}/${trim(path, '/')}?api_key=${API_KEY}`;
  const queryParams = [];

  for (let key of Object.keys(params)) {
    const value = params[key];
    queryParams.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
  }

  return uri + (queryParams.length ? `&${queryParams.join('&')}` : '');
}

function transformResponseData(data) {
  return data.results.map((movie) =>
    pick(movie, [
      'id',
      'title',
      'overview',
      'poster_path',
      'release_date',
      'vote_average',
    ])
  );
}

export async function overfetch(path, params) {
  const response = await fetch(buildUri(path, params));
  const data = await response.json();

  if (response.ok) {
    return data.results ? transformResponseData(data) : data;
  }

  return Promise.reject(data.status_message);
}
