import { trim, makeElement, createText } from './utils.js';

function renderMovie(
  movie,
  { imageBaseUri, isInFavorites, onToggleFavorites }
) {
  const root = makeElement('article', 'movie');

  const poster = makeElement('div', 'movie__poster');
  if (movie.poster_path) {
    poster.style.backgroundImage = `url(${trim(imageBaseUri, '/')}/w154/${trim(
      movie.poster_path,
      '/'
    )})`;
  }

  const rating = makeElement('span', 'movie__rating');
  rating.appendChild(createText(`${movie.vote_average * 10}%`));

  poster.appendChild(rating);
  root.appendChild(poster);

  const movieInfo = makeElement('div', 'movie__info');
  const releaseYear = makeElement('span', 'movie__year');
  releaseYear.appendChild(
    createText(` (${movie.release_date.substring(0, 4)})`)
  );

  const title = makeElement('h1', 'movie__title');
  title.appendChild(createText(movie.title));
  title.appendChild(releaseYear);
  movieInfo.appendChild(title);

  const overview = makeElement('p', 'movie__overview');
  overview.appendChild(createText(movie.overview));
  movieInfo.appendChild(overview);
  root.appendChild(movieInfo);

  const actions = makeElement('footer', 'movie__actions');
  const favoriteButton = makeElement(
    'button',
    `button button--fav ${isInFavorites ? 'button--fav-active' : ''}`
  );
  const icon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  icon.setAttribute('class', 'icon');

  const use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#favorite');
  icon.appendChild(use);
  favoriteButton.appendChild(icon);
  favoriteButton.addEventListener('click', () => {
    onToggleFavorites(movie.id);
  });
  actions.appendChild(favoriteButton);

  const moreButton = makeElement('a', 'button');
  moreButton.href = `https://www.themoviedb.org/movie/${movie.id}`;
  moreButton.appendChild(createText('More info'));
  actions.appendChild(moreButton);
  root.appendChild(actions);

  return root;
}

function renderMessage(message, className) {
  const element = makeElement('p', className);

  element.appendChild(createText(message));

  return element;
}

export function renderMoviesList(
  { isLoading, filtered: movies, errorMessage, favorites },
  imageBaseUri,
  onToggleFavorites
) {
  if (isLoading) {
    return renderMessage('Loading…', 'app-message');
  }

  if (errorMessage) {
    return renderMessage(errorMessage, 'app-message app-message--error');
  }

  const list = makeElement('ul', 'movies-list');

  movies.forEach((movie) => {
    const listItem = makeElement('li', 'movies-list__item');

    listItem.appendChild(
      renderMovie(movie, {
        imageBaseUri,
        onToggleFavorites,
        isInFavorites: favorites.includes(movie.id),
      })
    );
    list.appendChild(listItem);
  });

  return list;
}
