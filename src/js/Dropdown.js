const DROPDOWN_ANCHOR_SELECTOR = '.dropdown__anchor';
const DROPDOWN_LABEL_SELECTOR = '.dropdown__label';
const DROPDOWN_OPEN_CLASSNAME = 'dropdown--open';
const DROPDOWN_ITEM_SELECTOR = '.dropdown__menu-button';

export class Dropdown {
  constructor(selector, defaultValue) {
    this.root = document.querySelector(selector);
    this.value = defaultValue;

    if (!this.root) {
      throw new Error(`Could not find dropdown root element at '${selector}'`);
    }

    this.anchor = this.root.querySelector(DROPDOWN_ANCHOR_SELECTOR);

    if (!this.anchor) {
      throw new Error(
        `Could not find dropdown anchor element at '${selector} ${DROPDOWN_ANCHOR_SELECTOR}'`
      );
    }

    this.label = this.anchor.querySelector(DROPDOWN_LABEL_SELECTOR);

    if (!this.label) {
      throw new Error(
        `Could not find dropdown label element at '${selector} ${DROPDOWN_ANCHOR_SELECTOR} ${DROPDOWN_LABEL_SELECTOR}'`
      );
    }

    this.anchor.addEventListener('click', this.toggleDropdown);
    this.items = Array.from(this.root.querySelectorAll(DROPDOWN_ITEM_SELECTOR));
  }

  toggleDropdown = (event) => {
    if (event) {
      event.stopPropagation();
    }

    if (this.root.classList.contains(DROPDOWN_OPEN_CLASSNAME)) {
      this.root.classList.remove(DROPDOWN_OPEN_CLASSNAME);
      // cleanup - we don't need these listeners anymore
      this.removeItemListeners();
      document.removeEventListener('click', this.handleClickOutside);
    } else {
      // here we add event listeners to items BEFORE desplaying a menu
      // because we want to be sure that items are available as soon as possible
      this.addItemListeners();
      this.root.classList.add(DROPDOWN_OPEN_CLASSNAME);
      document.addEventListener('click', this.handleClickOutside);
    }
  };

  handleClickOutside = (event) => {
    if (!this.root.contains(event.target)) {
      this.toggleDropdown();
    }
  };

  handleItemClick = (event) => {
    this.value = event.target.getAttribute('data-value');
    this.label.textContent = event.target.textContent;
    this.toggleDropdown();

    if (this.onChange) {
      this.onChange(this.value);
    }
  };

  addItemListeners() {
    this.items.forEach((item) => {
      item.addEventListener('click', this.handleItemClick);
    });
  }

  removeItemListeners() {
    this.items.forEach((item) => {
      item.removeEventListener('click', this.handleItemClick);
    });
  }

  setChangeListener(listener) {
    if ('function' !== typeof listener) {
      throw new TypeError('Change listener must be a function');
    }

    this.onChange = listener;
  }

  getValue() {
    return this.value;
  }
}
