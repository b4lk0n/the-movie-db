import { SearchField } from './SearchField.js';
import { Dropdown } from './Dropdown.js';
import { Filters } from './Filters.js';
import { App } from './App.js';

/*
  to make sure that we the application is launched when DOM is ready
  there is another benefit — all modules are loaded, compiled and resolved
  but the will be executed only after "DOMContentLoaded" event
*/
document.addEventListener('DOMContentLoaded', async () => {
  const searchField = new SearchField('.search-field');
  const sortBy = new Dropdown('#sortBy', 'none');
  const showMode = new Dropdown('#showMode', 'all');

  const filters = new Filters(sortBy, showMode, searchField);

  await new App(filters, {
    root: document.querySelector('.app-main'),
    filterTitle: document.querySelector('.app-filters__title'),
    moviesCountFiltered: document.querySelector('.movies-count--filtered'),
    moviesCountTotal: document.querySelector('.movies-count--total'),
  }).start();
});
