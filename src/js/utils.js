/**
 * Returns a debounced function
 *
 * @param {Function} func Function to be debounced
 * @param {number} delay Execution detay in milliseconds
 * @returns {Function}
 */
export function debounce(func, delay) {
  let timeoutId;

  return (...args) => {
    const next = () => func(...args);
    clearTimeout(timeoutId);
    timeoutId = setTimeout(next, delay);
  };
}

/**
 * Removes characters from the beginning of a string
 * @param {String} string
 * @param {String} [chars]
 * @returns {String}
 */
export function trimStart(string, chars = '\\s') {
  const type = typeof string;

  if (type !== 'string') {
    throw new TypeError(`Only string values can be trimmed, provided: ${type}`);
  }

  return string.replace(new RegExp(`^[${chars}]+`), '');
}

/**
 * Removes characters from the end of a string
 * @param {String} string
 * @param {String} [chars]
 * @returns {String}
 */
export function trimEnd(string, chars = '\\s') {
  const type = typeof string;

  if (type !== 'string') {
    throw new TypeError(`Only string values can be trimmed, provided: ${type}`);
  }

  return string.replace(new RegExp(`[${chars}]+$`), '');
}

/**
 * Removes characters from the both ends of a string
 * @param {String} string
 * @param {String} [chars]
 * @returns {String}
 */
export function trim(string, chars) {
  return trimEnd(trimStart(string, chars), chars);
}

/**
 * Creates an object composed of the picked fields
 *
 * @param {Object} object
 * @param {String[]} fields
 * @returns {Object}
 */
export function pick(object, fields = []) {
  const picked = {};

  for (let key of Object.keys(object)) {
    if (fields.includes(key)) {
      picked[key] = object[key];
    }
  }

  return picked;
}

/**
 * Formats date object to a format that API understands
 * @param {Date} date
 * @returns {String}
 */
export function toApiDate(date) {
  if (!(date instanceof Date)) {
    throw new TypeError('Date must be an instance of class Date');
  }

  return [
    date.getFullYear(),
    `${date.getMonth() + 1}`.padStart(2, '0'),
    `${date.getDate()}`.padStart(2, '0'),
  ].join('-');
}

/**
 * Creates HTML element and sets className property
 *
 * @param {String} type
 * @param {String} [className]
 * @returns {HTMLElement}
 */
export function makeElement(type, className) {
  const element = document.createElement(type);

  if (className) {
    element.className = className;
  }

  return element;
}

/**
 * Creates Text node
 *
 * @param {string} text
 * @returns {Text}
 */
export function createText(text) {
  return document.createTextNode(text);
}
