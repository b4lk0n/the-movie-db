import { overfetch } from './overfetch.js';
import { toApiDate } from './utils.js';

export async function getImagesBaseUri() {
  const response = await overfetch('configuration');

  return response.images.base_url;
}

export function getRecentMovies(isSortByRating = false) {
  const now = new Date();
  const threeMonthsAgo = new Date(now.getTime());
  threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);

  const params = {
    'primary_release_date.lte': toApiDate(now),
    'primary_release_date.gte': toApiDate(threeMonthsAgo),
    sort_by: isSortByRating ? 'vote_average.desc' : 'primary_release_date.desc',
    'vote_count.gte': 50,
  };

  return overfetch('discover/movie', params);
}

export function searchMovie(query) {
  return overfetch('search/movie', { query });
}
