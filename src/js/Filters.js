import { Dropdown } from './Dropdown.js';
import { SearchField } from './SearchField.js';

export class Filters {
  constructor(sortBy, showMode, searchField) {
    if (!(sortBy instanceof Dropdown)) {
      throw new TypeError(
        'Sort by dropdown must be an instance of Dropdown class'
      );
    }

    this.sortBy = sortBy.getValue(); // initial value
    sortBy.setChangeListener(this.handleFilterChange('sortBy'));

    if (!(showMode instanceof Dropdown)) {
      throw new TypeError(
        'Show mode dropdown must be an instance of Dropdown class'
      );
    }

    this.showMode = showMode.getValue(); // initial value
    showMode.setChangeListener(this.handleFilterChange('showMode'));

    if (!(searchField instanceof SearchField)) {
      throw new TypeError(
        'Search field must be an instance of SearchField class'
      );
    }

    this.searchQuery = searchField.getValue(); // initial value
    searchField.setChangeListener(this.handleFilterChange('searchQuery'));
  }

  handleFilterChange = (filterName) => (value) => {
    if (this[filterName] !== value) {
      this[filterName] = value;
      this.notifyChange();
    }
  };

  setChangeListener(listener) {
    if ('function' !== typeof listener) {
      throw new TypeError('Change listener must be a function');
    }

    this.onChange = listener;
  }

  getValue() {
    return {
      sortBy: this.sortBy,
      showMode: this.showMode,
      searchQuery: this.searchQuery,
    };
  }

  notifyChange() {
    if (this.onChange) {
      this.onChange(this.getValue());
    }
  }
}
